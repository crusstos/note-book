﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBook
{
    class Note
    {
        public int id;
        public static int count = 0;
        public string name;
        public string surname;
        public string middleName;
        public long number;
        public string country;
        public string rabota;
        public string prochee;
        public string doljnost;
        public DateTime birthday;
        public Note(string surname, string name, string middleName, long number, string country, string doljnost, string rabota, string prochee, DateTime birthday)
        {
            count++;
            id = count;
            this.name = name;
            this.surname = surname;
            this.number = number;
            this.country = country;
            if (middleName.Trim() != "")
            {
                this.middleName = middleName;
            }
            if (country.Trim() != "")
            {
                this.country = country;
            }
            if (doljnost.Trim() != "")
            {
                this.doljnost = doljnost;
            }
            if (prochee.Trim() != "")
            {
                this.prochee = prochee;
            }
            if (rabota.Trim() != "")
            {
                this.rabota = rabota;
            }
            if (birthday != new DateTime())
            {
                this.birthday = birthday;
            }
        }

        public void Edit()
        {
            List<string> polya = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9"};
            Console.WriteLine("введите номер поля который хотите редактировац");
            Console.WriteLine("1. Имя");
            Console.WriteLine("2. Фамилия");
            Console.WriteLine("3. Отчество");
            Console.WriteLine("4. Номер");
            Console.WriteLine("5. Страна");
            Console.WriteLine("6. Организация");
            Console.WriteLine("7. Должность");
            Console.WriteLine("8. Прочее");
            Console.WriteLine("9. Дата рождения");
            string n = Console.ReadLine();
            while (!polya.Contains(n))
            {
                Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз");
                n = Console.ReadLine();
            }

            switch (n)
            {
                case "1":
                    Console.WriteLine("впишите имя");
                    string name = Console.ReadLine();
                    if (name != "")
                    {
                        this.name = name;
                    }
                    else
                    {
                        Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз");
                    }
                    break;
                case "2":
                    Console.WriteLine("впишите фамилию");
                    string surname = Console.ReadLine();
                    if (surname != "")
                    {
                        this.surname = surname;
                    }
                    else
                    {
                        Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз");
                    }
                    break;
                case "3":
                    Console.WriteLine("впишите отчество");
                    string middleName = Console.ReadLine();
                    this.middleName = middleName;
                    break;
                case "4":
                    Console.WriteLine("впишите имя");
                    string number = Console.ReadLine();
                    if (number != "")
                    {
                        try
                        {
                            this.number = Convert.ToInt64(number);
                        }
                        catch (Exception)
                        {

                            Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз");
                        }
                    }
                    else
                    {
                        Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз");
                    }
                    break;
                case "5":
                    Console.WriteLine("впишите страну");
                    string country = Console.ReadLine();
                    if (country != "")
                    {
                        this.country = country;
                    }
                    else
                    {
                        Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз");
                    }
                    break;
                case "6":
                    Console.WriteLine("впишите название организации");
                    string rabota = Console.ReadLine();
                    this.rabota = rabota;
                    break;
                case "7":
                    Console.WriteLine("впишите должность");
                    string doljnost = Console.ReadLine();
                    this.doljnost = doljnost;
                    break;
                case "8":
                    Console.WriteLine("впишите прочие заметки");
                    string prochee = Console.ReadLine();
                    this.prochee = prochee;
                    break;
                case "9":
                    Console.WriteLine("впишите дату рождения");
                    string birthday = Console.ReadLine();
                    try
                    {
                        this.birthday = Convert.ToDateTime(birthday);
                    }
                    catch (Exception)
                    {

                        Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз");
                    }
                    break;
                default:
                    break;
            }
        }

        public void Show()
        {
            if (birthday != new DateTime())
            {
                Console.WriteLine(id + ". " + name + " " + surname + " " + middleName + ". Тел.: " + number + " " + country + " " + rabota + " " + doljnost + " " + " " + prochee + " " + birthday);
            }
            else Console.WriteLine(id + ". " + name + " " + surname + " " + middleName + ". Тел.: " + number + " " + country + " " + rabota + " " + doljnost + " " + prochee);
        }
        public override string ToString()
        {
            return $"{this.id}. {this.surname} {this.name}. Тел.: {this.number}.";
        }
    }
}
