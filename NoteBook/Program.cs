﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBook
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] spisokKomand = new string[5] { "1", "2", "3", "4", "5" };

            List<Note> notes = new List<Note>();

            while (true)
            {
                Console.Clear();
                Console.WriteLine("хотите чето в записной книжке посмотреть вам сюда");
                Console.WriteLine("1. добавить запись");
                Console.WriteLine("2. редактировац запись");
                Console.WriteLine("3. удалить запись");
                Console.WriteLine("4. посмотрец все записи");
                Console.WriteLine("5. посмотреть кратенько самое важное в записи записи");
                string komanda = Console.ReadLine();
                if (!spisokKomand.Contains(komanda))
                {
                    Console.WriteLine("не пишите сюда больше");
                    Console.ReadKey();
                    continue;
                }
                switch (komanda)
                {
                    case "1":
                        Console.WriteLine("щас имя надо вводить");
                        string name = Console.ReadLine();
                        if (name == "")
                        { Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз"); Console.ReadKey(); continue; }
                        Console.WriteLine("щас фамилию надо вводить");
                        string surname = Console.ReadLine();
                        if (surname == "")
                        { Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз"); Console.ReadKey(); continue; }
                        Console.WriteLine("щас отчество надо вводить, если надо");
                        string middleName = Console.ReadLine();
                        Console.WriteLine("щас номер надо вводить");
                        long number;
                        try
                        {
                            number = Convert.ToInt64(Console.ReadLine());
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз");
                            Console.ReadKey();
                            continue;
                        }
                        Console.WriteLine("щас страну вводить");
                        string country = Console.ReadLine();
                        if (country == "")
                        { Console.WriteLine("ну давайте с вами почеловечески поступим ну впишите пожалуйста в следующий раз"); Console.ReadKey(); continue; }
                        Console.WriteLine("щас когда др надо вводить, если надо");
                        string bd = Console.ReadLine();
                        DateTime birthday;
                        if (bd == "")
                            birthday = new DateTime();
                        else
                            birthday = Convert.ToDateTime(bd);
                        Console.WriteLine("щас организацию надо вводить, если надо");
                        string rabota = Console.ReadLine(); 
                        Console.WriteLine("щас должность надо вводить, если надо");
                        string doljnost = Console.ReadLine(); 
                        Console.WriteLine("щас прочие заметки надо вводить, если надо");
                        string prochee = Console.ReadLine();
                        notes.Add(new Note(surname, name, middleName, number, country, doljnost, rabota, prochee, birthday));
                        break;
                    case "2":
                        Console.WriteLine("впишите короче id того кого хотите редактировац");
                        int index_ = Convert.ToInt32(Console.ReadLine());
                        notes[index_ - 1].Edit();
                        break;
                    case "3":
                        Console.WriteLine("впишите короче id того кого хотите удалиц");
                        int index = Convert.ToInt32(Console.ReadLine());
                        notes.RemoveAt(index);
                        break;
                    case "4":
                        Console.Clear();
                        if (notes.Count > 0)
                        {
                            foreach (var item in notes)
                            {       
                                item.Show();
                            }
                        }
                        else
                        {
                            Console.WriteLine("чето пока ничего не подвезли ждите...");
                        }
                        Console.ReadKey();
                        break;
                    case "5":
                        Console.Clear();
                        if (notes.Count > 0)
                        {
                            foreach (var item in notes)
                            {
                                Console.WriteLine(item);
                            }
                        }
                        else
                        {
                            Console.WriteLine("чето пока ничего не подвезли ждите...");
                        }
                        Console.ReadKey();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
